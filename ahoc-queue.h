// Basic queue implementation using a doubly-linked list

#ifndef AHOC_QUEUE_H
#define AHOC_QUEUE_H

#include "aho-corasick.h"

//--- STRUCT DEFINITIONS ---//

struct QueueNode {
  struct QueueNode *next;
  struct QueueNode *prev;
  StateNode *state;
};

struct Queue {
  struct QueueNode *head;
  struct QueueNode *tail;
};

//-- TYPE DEFINITIONS --//
typedef struct QueueNode QNode;
typedef struct Queue Queue;

//--- CONSTRUCTOR DECLARATIONS ---//
QNode *createQNode();

Queue *createQueue();

//--- DESTRUCTOR DECLARATIONS ---//

void deleteQNode(QNode **node);

void deleteQueue(Queue **queue);

//--- USER FUNCTIONS ---//

int queuePush(Queue *queue, StateNode *state);

StateNode *queuePop(Queue *queue);

/*
//-- HELPER FUNCTIONS --//
void printQueue(Queue *queue)
{
  QNode *temp = queue->tail;
  while (temp->prev != NULL) {
    printf("state: %d\n", temp->state->state);
    temp = temp->prev;
  }
  printf("state: %d\n", temp->state->state);
}

//-- DRIVER PROGRAM (TESTING) --//
int main(int argc, char *argv[])
{
  Queue *queue = createQueue();

  for (int i = 0; i < 100; i++) {
    queuePush(queue, i);
  }

  while (queue->tail->state < 50) {
    printf("popped state: %d\n", queuePop(queue));
  }

  printQueue(queue);

  deleteQueue(queue);

  return 0;
}
*/

#endif
