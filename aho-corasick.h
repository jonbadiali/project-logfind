// Header file for implementation of Aho-Corasick algorithm.
// Implementation stored in aho-corasick.c.

#ifndef AHO_CORASICK_H
#define AHO_CORASICK_H

#include <stdio.h>

enum { MAX_CHARS = 256 };   // Number of characters in character set, e.g. 256 for extended ASCII

//--- STRUCTURE DEFINITIONS ---//

// Define type for individual nodes of the state machine constructed by the Aho-Corasick algorithm.
// Each node represents a single state of the machine and stores all state-related data.
struct StateMachineNode {
  struct StateMachineNode *parent;               // Link to parent node
  struct StateMachineNode *children[MAX_CHARS];  // Array of links to child nodes indexed by ASCII code
  struct StateMachineNode *suffixLink;           // Link to longest suffix that is also a prefix of a keyword,
                                                 // i.e. "failure state"

  char edgeLabel;    // Symbol/character on preceding edge

  int stateId;       // State number
  int wordId;        // Index of keyword list for matching states to full keywords
};

// Single node of linked list for output function. Stores one keyword and pointer
// to next output node, if any.
struct OutputNode {
  struct OutputNode *next;
  char *keyword;
  int wordId;
};

// Linked list for storing output function values. One list is assigned per state
// in the graph, i.e. one per array index.
struct OutputList {
  struct OutputNode *head;
  struct OutputNode *tail;
};

// Structure representing a complete state machine constructed according to the Aho-Corasick algorithm.
//
// The three functions of the original algorithm ("goto", "failure", and "output") have been revised
// for implementation in the following manner:
//
// The "goto function" is stored as a directed graph from a single root node. Since the machine is
// always traversed sequentially, state-by-state, representing the goto function as a literal function
// is unnecessary. Rather, state transitions are managed by an indepent, user-facing search function
// (multiFileSearch).
//
// The "failure function", on the other hand, is preserved as a separate array suffix nodes.
struct StateMachine {
  struct StateMachineNode *root;
  struct OutputList **output;           // Pointer to output function array of output lists
  struct StateMachineNode **failure;    // Pointer to failure function array of suffix nodes

  char **keywords;              // Array of keywords for direct access via wordId
  int totalKeywords;

  int totalStates;              // Total number of machine states, INCLUDING start = 0
};

// Each MatchContainer stores the match data (or lack therof) for a given keyword on the input text.
// This data includes a dynamic array containing an ordered series of line numbers
// where a keyword was found along with the total number of matches for that keyword.
// The size and tail of the dynamic array are also stored for ease of maintenance.
struct MatchContainer {
  int *lineNumbers;
  int size;
  int tail;
  int totalMatches;
};

//- TYPE DEFINITIONS -//

typedef struct StateMachineNode StateNode;
typedef struct StateMachine StateMachine;
typedef struct MatchContainer Match;

//--- CONSTRUCTOR DECLARATIONS---//

// Construct new goto function by following Algorithm 2 of Aho-Corasick.
StateMachine *constructStateMachine(char **keywords, int num_keywords);

// Create new node in state machine used by Aho-Corasick algorithm
StateNode *createStateNode(StateNode *parent, char symbol, int state);

// Create new OutputList and initialize by creating a new head node with keyword.
struct OutputList *createOutputList(char *keyword, int keyword_index);

// Create new OutputNode and initialize with keyword.
struct OutputNode *createOutputNode(char *keyword, int keyword_index);

// Create array of dynamic arrays to store matching line numbers for each keyword
Match **createMatchArray(int num_keywords);

// Create a single Match and initialize members to 0.
Match *createMatch(void);

//--- DESTRUCTOR DECLARATIONS ---//

void cleanStateMachine(StateMachine *tree);

void cleanStateNode(StateNode *node);

void cleanOutputList(struct OutputList *output_list);

void cleanOutputNode(struct OutputNode *output_node);

void cleanMatchArray(Match **matches, int num_keywords);

void cleanMatch(Match *match);

//--- ALGORITHM FUNCTIONS ---//

// Subroutine of "Algorithm 2" from Aho-Corasick that...
int enter(char *keyword, int index, int *curr_state_id, StateMachine *new_machine);

// Helper function to check whether a given state-character pair results
// in a fail outcome, i.e. the character does not exist on a child edge
// from state.
// Syntax modeled after goto function of "Algorithm 2": g(state, a).
// Returns 1 if the pair results in a "fail" and 0 if not.
int isFail(StateNode *state, char a);

// Append output node representing state to output list
int addOutputNode(StateMachine *new_machine, StateNode *state);

// Construct failure function using "Algorithm 3" of Aho-Corasick.
int constructFailureF(StateMachine *new_machine);

//--- USER FUNCTIONS ---//

// Searches given file stream for set of keywords using the Aho-Corasick
// substring search algorithm and prints matches by line number.
int fileSearch(FILE *stream, char **keywords, int num_keywords);

// Search multiple text files for given keywords by creating state machine for
// keywords and running each input text file from in pathnames through it
int multiFileSearch(char **pathnames, int num_paths, char **keywords, int num_keywords, int *options);

//-- SUPPORT FUNCTIONS --//

// Adds current line number to dynamic match array of each keyword in the passed output list
// and increments the total number of matches for that keyword.
int addOutputToMatches(struct OutputList *list, Match **matches, int lineno);

// Pushes a new line number to the tail a dynamic array in the matches array
int pushLineNo(Match **matches, int index, int lineno);

// Print match data if any keyword matches (OR logic)
void printMatchesO(StateMachine *stateMachine, Match **matches);

// Print match data if all keywords match (AND logic)
void printMatchesA(StateMachine *stateMachine, Match **matches);

//-- OLD TEST FUNCTIONS --//

// Prints all keyword matches for a given state from output function, including line number
void printOutputList(struct OutputList *list, int lineno);

// Prints tree in depth-first order
void DFS_print(StateMachine *tree, StateNode *node);

// Prints the output function
void output_print(StateMachine *tree);

#endif
