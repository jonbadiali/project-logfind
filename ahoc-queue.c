// Basic queue implementation using a doubly-linked list to
// store nodes of the suffix tree created in aho-corasick.c.
//
// The queue is used principally to track matches for the
// purpose of formatting output.

#include <stdlib.h>
#include "ahoc-queue.h"
#include "aho-corasick.h"
#include "dbg.h"

//--- CONSTRUCTOR DEFINITIONS ---//

// Preconditions: none
// Postconditions: a new queue node has been allocated and
// all of its values initialized to NULL
QNode *createQNode()
{
  QNode *new_node = malloc(sizeof(struct QueueNode));     // Allocate new queue node
  check((new_node != NULL), "Failed to allocate new QNode");

  new_node->next = NULL;        // Initialize links to NULL
  new_node->prev = NULL;
  new_node->state = NULL;       // Initialize value (state node pointer) to NULL
  
  return new_node;
  
error:
  return NULL;
}

// Preconditions: none
// Postconditions: a new queue is allocated and all if its
// members are initialized to NULL
Queue *createQueue()
{
  Queue *new_queue = malloc(sizeof(struct Queue));    // Allocate new queue
  check((new_queue != NULL), "createQueue: failed to allocate new QNode");

  new_queue->head = NULL;     // Initialize head and tail to NULL
  new_queue->tail = NULL;

  return new_queue;

error:
  return NULL;
}

//--- DESTRUCTOR DEFINITIONS ---//

// Frees the memory allocated for a single queue node.
// Takes pointer-to-pointer to allow freed pointers to be set to NULL.
void deleteQNode(QNode **node) {
  if (*node) {
    free(*node);
    node = NULL;    // Reset pointer to NULL
    check(node == NULL, "failed to reset node to NULL");
  }
error:
  return;
}

// Frees the memory of all nodes in a queue along with the queue itself.
// Takes pointer-to-pointer to allow freed pointers to be set to NULL.
void deleteQueue(Queue **queue)
{
  Queue *temp_queue = *queue;               // Store Queue pointer in temporary shorthand
  QNode *temp_node = temp_queue->head;      // Store queue head in temporary shorthand

  if (temp_node != NULL) {                  // If the queue is not empty
    while (temp_node->next != NULL) {       // and the next node exists
      temp_node = temp_node->next;          // Traverse to next node
      free(temp_node->prev);                // and free the previous node

      temp_node->prev = NULL;               // Reset freed pointer to NULL
      check(temp_node->prev == NULL, "failed to reset previous node to NULL");
    }
    free(temp_node);        // Free the last node
    temp_node = NULL;       // and reset to NULL
  }
  free(*queue);   // Free the queue
  queue = NULL;   // and reset to NULL
error:
  return;
}

//--- USER FUNCTIONS ---//

// Preconditions: queue created and initialized, a non-empty node of the
// aho-corasick.c state machine is initialized with non-NULL values
// Postconditions: a new queue node is created at the end of the queue and
// its value is set to the address/pointer of the passed state node
//
// This function accounts for the cases where the queue is empty or has one element.
int queuePush(Queue *queue, StateNode *state)
{
  QNode *new_node = createQNode();    // Create new node to add to queue
  check((new_node != NULL), "queuePush: failed to create new QNode");

  new_node->state = state;      // Set value of new_node to passed state node

  if (queue->head == NULL) {    // If queue is empty
    queue->head = new_node;     // Set head and tail to new_node
    queue->tail = new_node;
  } else if (queue->tail == queue->head) {    // If there is one element in the queue
    queue->tail = queue->head;                // Push head forward to make it the tail
    queue->head = new_node;                   // Add the new_node at the head of the queue

    queue->tail->prev = queue->head;          // Link the tail back to the head
    queue->head->next = queue->tail;          // Link the head forward to the tail

  } else {                            // If there is more than one element in the queue
    new_node->next = queue->head;     // Link the new_node forward to current head
    queue->head->prev = new_node;     // Link the current head back to the new_node
    queue->head = new_node;           // Set the head to the new_node
  }

  return 0;

error:
  return 1;
}

// Preconditions: queue created and contains at least one element
// Postconditions: the first node is deleted from the queue and its value,
// the address of a state machine node, is returned
StateNode *queuePop(Queue *queue)
{
  check((queue->tail != NULL), "queuePop: pop failed: queue is empty");
  check((queue->tail->state != NULL), "queuePop: invalid target state (NULL)");
  StateNode *state = queue->tail->state;     // Store machine state of node to pop (tail)

  QNode *new_tail = queue->tail->prev;  // Store next to last node in temporary pointer
  if (new_tail != NULL) {               // If next to last node exists
    new_tail->next = NULL;              // Link it forward to NULL

  } else {                      // If next to last node does not exist
    queue->head = NULL;         // The queue is empty, set head to NULL to reflect this
  }

  deleteQNode(&(queue->tail));   // Remove tail from queue
  queue->tail = new_tail;        // Update the queue tail to the new_tail

  return state;     // Return the value of the popped node

error:
  return NULL;
}

/*
//-- HELPER FUNCTIONS --//
int printQueue(Queue *queue)
{
  QNode *temp = queue->tail;
  check((temp != NULL), "printQueue: queue is empty");
  while (temp->prev != NULL) {
    printf("state: %d\n", temp->state);
    //printf("state: %d\n", temp->state->state);
    temp = temp->prev;
  }
  printf("state: %d\n", temp->state);
  //printf("state: %d\n", temp->state->state);
  return 0;

error:
  return 1;
}
*/
